using System.Buffers.Binary;
using System.Runtime.InteropServices.ComTypes;
using Xunit;
using Xunit.Abstractions;

namespace EGE.Test
{
    public class Task_T1933
    {
        private readonly ITestOutputHelper _output;

        public Task_T1933(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData(new []
        {
            7, 58, 2, 3, 5, 4, 1, 29
        }, 6)]
        [InlineData(new []
        {
            116, 58, 2, 3, 5, 4, 1, 29
        }, 6+3)]
        [InlineData(new []
        {
            1, 2, 2, 3, 5, 4, 1, 1
        }, 0)]
        [InlineData(new []
        {
            1, 2, 2, 29, 5, 4, 1, 1
        }, 1)]
        public void Test(int[] source, int expected)
        {
            var clc = new Calculator(_output, 4, 29);
            
            var resultRaw = clc.CalculateRaw(source);
            var result = clc.Calculate(source);
            
            _output.WriteLine("Raw {0} Optimized {1}", resultRaw, result);

            Assert.Equal(expected, resultRaw);
            Assert.Equal(expected, result);
        }
    }
}