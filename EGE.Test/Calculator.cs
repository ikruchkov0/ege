using System;
using System.Collections.Generic;
using Xunit.Abstractions;

namespace EGE.Test
{
    // Решение для https://yandex.ru/tutor/subject/tag/problems/?ege_number_id=365&tag_id=19
    public class Calculator
    {
        private readonly ITestOutputHelper _output;
        private readonly int _distance;
        private readonly int _anchor;

        public Calculator(ITestOutputHelper output, int distance, int anchor)
        {
            _output = output;
            _distance = distance;
            _anchor = anchor;
        }

        public int Calculate(int[] source)
        {
            int counter = 0;
            int lastAnchorIndex = -1;
            int anchorPairsCount = 0;
            int anchorCount = 0; 
            for (int i = 0; i < source.Length; i++)
            {
                var a = source[i];
                if (a % _anchor != 0)
                {
                    continue;
                }
                
                if (lastAnchorIndex != -1 && Math.Abs(lastAnchorIndex - i) >= _distance) // могут создать пару
                {
                    anchorPairsCount += anchorCount; // все предыдущие тоже могут создать пару
                }

                lastAnchorIndex = i;
                
                anchorCount++;
                counter += pairsForIndex(i, source);
            }

            return counter - anchorPairsCount;
        }

        public int CalculateRaw(int[] source)
        {
            var paired = new HashSet<(int, int)>();
            int counter = 0;
            for (int i = 0; i < source.Length; i++)
            {
                for (var j = 0; j < source.Length; j++)
                {
                    if (Math.Abs(i - j) < _distance)
                    {
                        continue;
                    }

                    var a = source[i];
                    var b = source[j];
                    var mult = a * b;
                    if (mult % _anchor != 0)
                    {
                        continue;
                    }

                    if (paired.Contains((i, j)))
                    {
                        continue;
                    }

                    paired.Add((i, j));
                    paired.Add((j, i));
                    _output.WriteLine("{0} {1}", a, b);
                    counter++;
                }
            }

            return counter;
        }

        private int pairsForIndex(int index, int[] source)
        {
            var deadZoneCount = _distance - 1; // кол-во элементов сверху и снизу не входящие в пары
            var sourceBottomBoundary = source.Length - 1;

            var top = index - deadZoneCount; // считаем число элементов сверху
            if (top < 0) // если вышли за границу сверху
            {
                top = 0; // корректируемся
            }

            var bottom = sourceBottomBoundary - (index + deadZoneCount); // считаем кол-во элементов снизу
            if (bottom < 0) // если вышли за границу сверху
            {
                bottom = 0; // корректируемся
            }

            var result = top + bottom;
            
            _output.WriteLine($"[{index} ({source[index]})] Pairs top {top} + bottom {bottom} {result}");

            return result;
        }
    }
}